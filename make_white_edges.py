#!/usr/bin/env python
# coding:utf-8
from PIL import Image, ImageChops, ImageCms
import sys
import argparse


PIXEL_KOEF = 25.4  # коэффициент конвертации мм в пиксели при ~300dpi
DPI = 300
DEFAULT_PROFILE_RGB = 'icc/sRGB IEC61966-21.icc'
DEFAULT_PROFILE_CMYK = 'icc/uswebcoatedswop.icc'


def tif2jpg(image_io, input_profile=None, output_profile=None):
    return ImageCms.profileToProfile(
        image_io,
        inputProfile=input_profile,
        outputProfile=output_profile,
        outputMode='RGB',
    )


def mm_to_pixels(mm, dpi=DPI):
    return int(round(mm / PIXEL_KOEF * dpi))


def make_preview(name, im, input_profile=None, output_profile=None):
    jpg = tif2jpg(im, input_profile, output_profile)
    dpi = DPI
    if jpg.width > mm_to_pixels(140) and jpg.height > mm_to_pixels(140):
        dpi = DPI / 2
        jpg = jpg.resize((jpg.width / 2, jpg.height / 2), Image.BICUBIC)
    jpg.save(name, dpi=(dpi, dpi), quality=85, subsampling=0)


def make_pdf(name, im):
    inv = ImageChops.invert(im)
    inv.save(name, "PDF", resolution=DPI, dpi=(DPI, DPI), quality=100,
             subsampling=0)


def _save(image_io, jpg, pdf, input_profile=None, output_profile=None):
    make_preview(jpg, image_io, input_profile, output_profile)
    make_pdf(pdf, image_io)


def _white_edges(image_io, bleeds, transform_bleed_fn=lambda x: 2 * x + 1):
    top, right, bottom, left = map(
        lambda s: mm_to_pixels(transform_bleed_fn(s)),
        bleeds
    )

    width, height = image_io.size
    resized = image_io.resize(
        (width - left - right, height - top - bottom),
        Image.BICUBIC,
    )
    width, height = resized.size

    white_edged = Image.new(
        mode=image_io.mode,
        size=(left + right + width, top + bottom + height),
        color=(0, 0, 0, 0)
    )
    white_edged.paste(
        resized,
        ((white_edged.width - width) / 2, (white_edged.height - height) / 2)
    )
    return white_edged


def main(sys_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file',
        nargs=1,
        type=str,
    )
    parser.add_argument(
        'jpg_output_file',
        nargs=1,
        type=str,
    )
    parser.add_argument(
        'pdf_output_file',
        nargs=1,
        type=str,
    )
    parser.add_argument(
        '-b',
        '--bleeds',
        type=float,
        nargs=4,
    )

    args = parser.parse_args(sys_args)

    input_file = args.input_file[0]
    jpg_output_file = args.jpg_output_file[0]
    pdf_output_file = args.pdf_output_file[0]

    bleeds = args.bleeds

    image_io = Image.open(input_file)

    input_profile = DEFAULT_PROFILE_CMYK
    output_profile = DEFAULT_PROFILE_RGB

    name, ext = input_file.split('.')

    kw = {
        'input_profile': input_profile,
        'output_profile': output_profile,
    }

    _save(_white_edges(image_io, bleeds),
          jpg_output_file, pdf_output_file, **kw)


if __name__ == "__main__":
    main(sys.argv[1::])
