#!/usr/bin/env python
# coding:utf-8
from PIL import Image, ImageChops, ImageCms
import sys
import argparse
import time


def timeit(method):

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print '%r %2.2f sec' % (method.__name__, te-ts)
        return result

    return timed


PIXEL_KOEF = 25.4  # коэффициент конвертации мм в пиксели при ~300dpi
DPI = 300
DEFAULT_PROFILE_RGB = ImageCms.ImageCmsProfile('icc/sRGB IEC61966-21.icc')
DEFAULT_PROFILE_CMYK = ImageCms.ImageCmsProfile('icc/uswebcoatedswop.icc')


class Error(Exception):
    pass


def tif2jpg(image_io, input_profile=None, output_profile=None):

    if image_io.mode != 'CMYK':
        raise Error("got not cmyk image.")

    return ImageCms.profileToProfile(
        image_io,
        inputProfile=input_profile,
        outputProfile=output_profile,
        outputMode='RGB',
    )


def mm_to_pixels(mm, dpi=DPI):
    return int(round(mm / PIXEL_KOEF * dpi))


@timeit
def make_preview(name, im, input_profile=None, output_profile=None):
    jpg = tif2jpg(im, input_profile, output_profile)
    dpi = DPI
    if jpg.width > mm_to_pixels(140) and jpg.height > mm_to_pixels(140):
        dpi = DPI / 2
        jpg = jpg.resize((jpg.width / 2, jpg.height / 2), Image.BICUBIC)
    jpg.save(name, dpi=(dpi, dpi), quality=85, subsampling=0)


@timeit
def make_pdf(name, im):
    inv = ImageChops.invert(im)
    inv.save(name, "PDF", resolution=DPI, dpi=(DPI, DPI), quality=100,
             subsampling=0)


@timeit
def _save(image_io, name, input_profile=None, output_profile=None):
    make_preview("%s_preview.jpg" % name, image_io, input_profile,
                 output_profile)
    make_pdf("%s.pdf" % name, image_io)


@timeit
def _white_edges(image_io, bleeds, transform_bleed_fn=lambda x: 2 * x + 1):
    top, right, bottom, left = map(
        lambda s: mm_to_pixels(transform_bleed_fn(s)),
        bleeds
    )

    width, height = image_io.size
    resized = image_io.resize(
        (width - left - right, height - top - bottom),
        Image.BICUBIC,
    )
    width, height = resized.size

    white_edged = Image.new(
        mode=image_io.mode,
        size=(left + right + width, top + bottom + height),
        color=(0, 0, 0, 0)
    )
    white_edged.paste(
        resized,
        ((white_edged.width - width) / 2, (white_edged.height - height) / 2)
    )
    return white_edged


@timeit
def _stretch_edges(image_io, bleeds, transform_bleed_fn=lambda x: x,
                   offset_mm=0.5):
    stretched = _white_edges(image_io, bleeds, transform_bleed_fn)
    width, height = stretched.size
    top, right, bottom, left = map(
        lambda s: mm_to_pixels(transform_bleed_fn(s)),
        bleeds
    )
    offset = mm_to_pixels(offset_mm)
    left_line = stretched.crop((
        left + offset,
        0,
        left + 2 * offset,
        height,
    )).resize((left + 2 * offset, height), Image.BILINEAR)

    right_line = stretched.crop((
        width - right - 2 * offset,
        0,
        width - right - offset,
        height,
    )).resize((right + 2 * offset, height), Image.BILINEAR)

    stretched.paste(left_line, (0, 0))
    stretched.paste(right_line, (width - right - 2 * offset, 0))

    top_line = stretched.crop((
        0,
        top + offset,
        width,
        top + 2 * offset,
    )).resize((width, top + 2 * offset), Image.BILINEAR)

    bottom_line = stretched.crop((
        0,
        height - bottom - 2 * offset,
        width,
        height - bottom - offset,
    )).resize((width, bottom + 2 * offset), Image.BILINEAR)

    stretched.paste(top_line, (0, 0))
    stretched.paste(bottom_line, (0, height - bottom - 2 * offset))

    return stretched


@timeit
def _double_stretch_edges(image_io, bleeds):
    return _stretch_edges(image_io, bleeds, lambda x: 2 * x)


def main(sys_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file',
        nargs=1,
        type=str,
        metavar='input tif filepath'
    )

    # bleeds
    parser.add_argument(
        '-tb',
        '--top-bleed-size',
        type=float,
        nargs='?',
        metavar='top bleed field size'
    )
    parser.add_argument(
        '-lb',
        '--left-bleed-size',
        type=float,
        nargs='?',
        metavar='left bleed field size'
    )
    parser.add_argument(
        '-rb',
        '--right-bleed-size',
        type=float,
        nargs='?',
        metavar='right bleed field size'
    )
    parser.add_argument(
        '-bb',
        '--bottom-bleed-size',
        type=float,
        nargs='?',
        metavar='bottom bleed field size'
    )
    parser.add_argument(
        '-b',
        '--bleed-size',
        type=float,
        nargs='?',
        metavar='set custom bleed size (default is 2 [mm])'
    )
    parser.add_argument(
        'fix',
        nargs=1,
        type=str,
        default='or',
        metavar='or | we | b2 | b4',
    )
    args = parser.parse_args(sys_args)

    b = args.bleed_size
    top = args.top_bleed_size
    right = args.right_bleed_size
    bottom = args.bottom_bleed_size
    left = args.left_bleed_size

    if b is None and any(map(lambda x: x is None, (top, left, right, bottom))):
        raise argparse.ArgumentTypeError("invalid bleed size.")

    top = top or b
    right = right or b
    bottom = bottom or b
    left = left or b

    bleeds = top, right, bottom, left

    input_file = args.input_file[0]

    image_io = Image.open(input_file)

    if image_io.mode != 'CMYK':
        raise Error("%s is not a cmyk image" % input_file)

    name, ext = input_file.split('.')

    input_profile = DEFAULT_PROFILE_CMYK
    output_profile = DEFAULT_PROFILE_RGB
    kw = {
        'input_profile': input_profile,
        'output_profile': output_profile,
    }

    _save(image_io, "%s_orig" % name, **kw)
    _save(_white_edges(image_io, bleeds), "%s_w" % name, **kw)
    _save(_stretch_edges(image_io, bleeds), "%s_b2" % name, **kw)
    _save(_double_stretch_edges(image_io, bleeds), "%s_b4" % name, **kw)


if __name__ == "__main__":
    main(sys.argv[1::])
