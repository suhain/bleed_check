#!/usr/bin/env python
# coding:utf-8
"""
Скрипт проверки вылетов.
На входе скрипт ожидает аргументы
input_file: путь к tif файлу, на котором будут проверяться вылеты
bleed_size: размер проверяемых вылетов в мм

На выходе скрипт пишет результат в stdout (True | False)
код возврата скрипта
0 в случае если вылеты в порядке
1 в случае когда вылеты не в порядке
"""
from PIL import Image
import sys
import argparse


MM_TO_PIXELS = 25.4  # коэффициент конвертации мм в пиксели при ~300dpi


def pixel_delta(pixel_a, pixel_b, channel_limit=8):
    """
    Проверяет разницу цветов для двух заданных пикселей по каждому каналу.
    pixel_a, pixel_b - пиксели, которые нужно сравнить
    channel_limit - предел абсолютной  разницы по каналу
    """
    return all(map(
        lambda channels: abs(channels[0] - channels[1]) < channel_limit,
        zip(pixel_a, pixel_b)
    ))


def _is_bleed_ok(image, bleeds_mm, start_with=1):
    """
    Функция проверки вылетов
    image - объект изображения PIL.Image instance
    bleeds_mm - размер вылета в мм
    start_with - с какого по счету пикселя начать итерацию проверки

    для каждой из сторон изображения проверяется крайняя полоса, по толщине
    равная размеру вылета, на наличие не однотонных объектов.

    Для ускорения процесса,
    изображение сжимается по ширине для проверки вертикальных вылетов
    изображение сжимается по высоте для проверки горизонтальных вылетов

    метод сжатия - билинейное сжатие
    """
    # посчитаем размер вылета в пикселях
    top, right, bottom, left = map(
        lambda s: int(2 * s / MM_TO_PIXELS * 40), bleeds_mm)
    # запишем реальные размеры изображения
    real_width, real_height = image.size

    # сжатие по ширине
    im = image.resize(
        (int(round(real_width / 30)), int(round(real_height / 7.5))),
        Image.BILINEAR
    )
    width, height = im.size

    def getpixel(x, y):
        return im.getpixel((x, y))

    # на каждой из итераций пропускается первый пиксель

    for x in xrange(start_with, width - start_with):
        for y in xrange(start_with, top - start_with):
            if not pixel_delta(
                getpixel(x, y),
                getpixel(x, y + 1)
            ):
                return False

    for x in xrange(start_with, width - start_with):
        for y in xrange(start_with, bottom - start_with):
            if not pixel_delta(
                getpixel(x, height - y - 1),
                getpixel(x, height - y - 2)
            ):
                return False

    # сжатие по высоте
    im = image.resize(
        (int(round(real_width / 7.5)), int(round(real_height / 30))),
        Image.BILINEAR
    )
    width, height = im.size

    for y in xrange(start_with, height - start_with):
        for x in xrange(start_with, left - start_with):
            if not pixel_delta(
                getpixel(x, y),
                getpixel(x + 1, y)
            ):
                return False

    for y in xrange(start_with, height - start_with):
        for x in xrange(start_with, right - start_with):
            if not pixel_delta(
                getpixel(width - x - 1, y),
                getpixel(width - x - 2, y)
            ):
                return False

    return True


def is_bleed_ok(path, bleeds=2.0):
    return _is_bleed_ok(Image.open(path), bleeds)


def main(sys_argv):
    parser = argparse.ArgumentParser(description='Process bleeds in tif file.')
    parser.add_argument(
        'input_file',
        nargs=1,
        type=str,
        metavar='input file'
    )
    parser.add_argument(
        '-b',
        '--bleeds',
        type=float,
        nargs=4,
    )
    args = parser.parse_args(sys_argv)
    result = is_bleed_ok(args.input_file[0], args.bleeds)
    # вывод результата в stdout
    print result

    sys.exit(0 if result else 1)


if __name__ == "__main__":
    main(sys.argv[1::])
